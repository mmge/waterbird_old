var express = require("express");
var path = require('path');

module.exports = function(wb) {

  var module = {};

  module.waterbird = wb;//connection to the data_entry_loop process;

  module.api = api = express(); // create the express server for api/websocket
  module.waterbird.setAPI(api); // attach the api to the data_entry_loop for broadcasting status updates;

  expressWs = require('express-ws')(api); // attach express-ws for websocket support

  module.wss = expressWs.getWss('/a'); // expose the websocket server for broadcasting messages

  api.broadcast = function(msg) {
    if(typeof msg === 'object') {
      msg = JSON.stringify(msg);
    }
    module.wss.clients.forEach(function(client) {
      client.send(msg);
    })
  }
/*
  api.use(swagger.init(api, {
    apiVersion: '0.1',
    swaggerVersion: '0.1',
    swaggerURL: '/swagger',
    swaggerJSON: '/.api-docs.json',
    swaggerUI: '/../public/swagger',
    basePath: 'http://localhost:9872',
    apis: ['/../apis.js', '/../apis.yml'],
    middleware: function(req, res) {next()}
  }))
*/
  api.use('/images', express.static(__dirname + '/../public/images/'));
  api.use('/swagger', express.static(__dirname + '/../public/swagger/'));

  api.ws('/socket', function(ws, req) {
    ws.on('message', function(msg) {
      if(msg === 'start') {
        module.waterbird.start();
        ws.send('started');
      } else if(msg === 'stop') {
        module.waterbird.stop();
        ws.send('stopped');
      } else if(msg === 'current') {
        ws.send(module.waterbird.current());
      } else if(msg === 'count') {
        ws.send(module.waterbird.count());
      } else if(msg === 'status') {
        ws.send(module.waterbird.status())
      }
      console.log(msg);
    })
  })

  api.get('/status', function(req, res) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify({'status': module.waterbird.status()}));

    /*
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end('Everything is perfectly alright now, We\'re fine... We\'re all fine here now, thank you. How are you?')
    */
  });

  api.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/../public/index.html'));
  });

  api.get('/count', function(req, res) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(JSON.stringify({'count': module.waterbird.count()}));
  });

  api.get('/current', function(req, res) {
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end(module.waterbird.current());
  });

  api.get('/start', function(req, res) {
    module.waterbird.start();
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end('{"status": "running"}');
  });

  api.get('/stop', function(req, res) {
    module.waterbird.stop();
    res.writeHead(200, {'Content-Type': 'application/json'});
    res.end('{"status": "stopped"}');
  })

  api.get("/kill", function(req, res) {
    res.writeHead(200, {'Content-Type': "application/json"});
    res.end('{"status": "dead"}');
  })

  module.start = function() {
    console.log("API Server Started...");
    api.listen(9872);
  }

  module.stop = function() {
    api.close();
  }

  return module;

};
