/**
* @fileOverview Create the waterbird object for OnCore data entry
* @author Eric Bailey
* @version 0.0.5
*/
'use strict';
const puppeteer = require('puppeteer');
const yaml = require('js-yaml');
const fs = require('fs');
const winston = require('winston');
const EventEmitter = require('events').EventEmitter;
const async = require('async');
const util = require('util');

// Helper for finding keys in an object
Object.prototype.has = function(key) {
  return Object.keys(this).indexOf(key) > -1;
};


/**
 * Waterbird - Main object of waterbird application
 * @constructor
 * @param  {object} options use to pass initialization options to Waterbird
 */
function Waterbird(options) {
  let wb = this;
  options = options || {};
  let config_file = options.config_file || 'config.yaml';
  wb.queue = async.queue(function(data, callback) {
    try {
      let task_name = data.type + '_' + data.target;
      let task_data = data.data;
      let task = new Task({name: task_name, data: task_data});
      task.on('complete', function() {
        setTimeout(callback, 100);
      });
      task.execute();
    } catch(err) {
      throw err;
    }
  }, 1);
  wb.queue.drain = function() {
    wb.emit('queue_empty');
  };
  wb.queue.error = function(error, task) {
    wb.emit('task_error', task);
    throw error;
  };

  let config = {debug: false,
                env: 'tst',
                viewport: {
                  width: 1920,
                  height: 1080},
               };


  if(fs.existsSync(config_file)) {
    config = Object.assign(config,
                           yaml.safeLoad(fs.readFileSync(config_file))
                         );
  }

  if(typeof options === 'object') {
    config = Object.assign(config, options);
  }

  if(!config.has('credentials')) {
    throw new Error('Please provide OnCore credentials in `options` or the ' +
     'configuration file.');
  }

  config.base_url = config.environments[config.env];

  this.get = function(key) {
    if(key !== 'password') {
      if(config.has(key)) {
        return config[key];
      } else {
        throw new Error('cannot get `' + key + '` from waterbird object.');
      }
    } else {
      throw new Error('cannot get `' + key + '` from waterbird object.');
    }
  };

  this.log = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
      new winston.transports.File({filename: './logs/error.log',
                                   level: 'error'}),
      new winston.transports.File({filename: './logs/combined.log'}),
    ],
  });
  // When debug === true output all logging to the console
  if(config.debug) {
    this.log.add(new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp: true,
      format: winston.format.simple(),
    }));
  }

  this.initialize = async function() {
    wb.log.info('puppeteer initialization started');
    wb.browser = await puppeteer.launch({
      headless: !config.debug,
      devtools: config.debug,
      ignoreHTTPSErrors: true,
    });
    wb.page = await wb.browser.newPage();
    await wb.page.setViewport({
      width: config.viewport.width,
      height: config.viewport.height,
    });
    wb.page.on('dialog', function(msg) {
      wb.log.error(msg);
      wb.emit('error', msg);
    });
    wb.emit('initialized');
  };

  this.captureMessages = async function(step) {
    let result = {errors: [], messages: []};
    let error = await wb.page.evaluate(() => {
      try {
        return document.querySelector('#errorMsg').innerText.trim().split('\n');
      } catch(err) {
        return [''];
      }
    });
    let message = await wb.page.evaluate(() => {
      try {
        return document.querySelector('#successMsg')
          .innerText.trim().split('\n');
      } catch(err) {
        return [''];
      }
    });
    if(error.length > 0 & error[0] !== '') {
      for(let e = 0; e < error.length; e++) {
        wb.emit('error', {message: error[e], step: step});
      }
      result.errors = error;
    }
    if(message.length > 0 & message[0] !== '') {
      for(let m = 0; m < message.length; m++) {
        wb.emit('message', {message: message[m], step: step});
      }
      result.messages = message;
    }
    return result;
  };
  wb.on('step_complete', async function(step) {
    await wb.captureMessages(step);
  });

  this.login = function() {
    wb.log.debug('Login requested');
    this.addTask({
      target: 'in',
      type: 'log',
      data: config.credentials,
    });
  };

  this.addTask = function(data) {
    wb.queue.push(data);
    wb.log.debug('task added');
    wb.emit('task_added');
  };

  this.close = async function(wait = 5000) {
    wb.log.info('Closing Waterbird session');
    wb.emit('waterbird_closing');
    await wb.page.waitFor(wait);
    await wb.browser.close();
  };

  const Task = require('./task.js')(wb);

  EventEmitter.call(this);
}
util.inherits(Waterbird, EventEmitter);

module.exports = Waterbird;
