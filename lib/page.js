const EventEmitter = require('events').EventEmitter;
const async = require('async');
const util = require('util');

module.exports = function(wb) {
  const Step = require('./step.js')(wb);


  /**
   * Page - Page object Constructor
   * @constructor
   *
   * @param  {object} args arguments used to construct the page object
   * @yields {object} an object that represents a waterbird page
   */
  function Page(args) {
    try {
      wb.log.verbose('New Page Initialized', args);

      let page = this;
      wb.log.verbose('New Page Built');

      if(args.has('config')) {
        page.config = args.config;
      } else {
        throw new Error('Step missing `config` arg');
      }

      if(args.has('data')) {
        page.data = args.data;
      } else {
        throw new Error('Step missing `data` arg');
      }

      page.queue = async.queue(function(config, callback) {
        try {
          wb.log.debug('Queue: ' + page.config.name);
          let step = new Step({config: config, data: page.data});
          step.on('complete', function() {
            setTimeout(callback, 100);
          });
          step.execute();
        } catch(err) {
          throw err;
        }
      });

      page.queue.drain = function(err) {
        if(page.config.has('wait')) {
          wb.page.waitFor(page.config.wait);
        }
        page.emit('complete', page);
      };
      page.queue.error = function(err) {
        throw err;
      };

      page.execute = async function() {
        try {
          wb.log.verbose('Executing page:' + page.config.name);
          let required = false;
          for(let i = 0; i < page.config.steps.length; i++) {
            if(page.config.steps[i].has('value')) {
              required = true;
              break;
            } else if(page.config.steps[i].has('source')) {
              if(page.data.has(page.config.steps[i].source)) {
                required = true;
                break;
              }
            }
          }
          if(required) {
            if(page.config.has('url')) {
              await wb.page.goto(page.config.url);
            }
            page.queue.push(page.config.steps, function() {
              console.log('Page Queue Callback');
            });
          } else {
            page.emit('skipped', page);
            page.emit('complete', page);
          }
        } catch(err) {
          throw err;
        }
      };

      EventEmitter.call(this);
    } catch(err) {
      throw err;
    }
  }
  util.inherits(Page, EventEmitter);

  return Page;
};
