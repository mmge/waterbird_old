const async = require('async');
const util = require('util');
const EventEmitter = require('events').EventEmitter;

module.exports = function(wb) {
  /**
   * Input - Input object Constructor
   * @constructor
   * @param  {object} args arguments used to construct the Input object
   * @yields {object} an object that represents a waterbird Input
   */
  function Input(args) {
    try {
      wb.log.debug('Input Created:', args);

      let input = this;

      if(args.has('selector')) {
        input.selector = args.selector;
      } else if(args.has('name')) {
        let tag = args.tag || '';
        input.selector = tag + '[name="' + args.name + '"]';
      } else if(args.has('id')) {
        input.selector = '#' + args.id;
      } else {
        throw new Error('Input missing identifier',
                        {args: args, obj: input});
      }

      input.checks = {
        dojo_select: async function(selector, value) {
          return true;
        },
        text: async function(selector, value) {
          return true;
        },
        button: async function(selector, value) {
          return true;
        },
        checkbox: async function(selector, value) {
          return true;
        },
        select: async function(selector, value) {
          return true;
        },
        checkbox_group: async function(selector, value) {
          return true;
        },
        radio_buttons: async function(selector, value) {
          return true;
        },
        textarea: async function(selector, value) {
          return true;
        },
      };

      input.actions = {
        dojo_select: async function(selector, value) {
          wb.log.debug('inside dojo_select: ' + selector + ' | ' + value);
          await wb.page.type(selector+'+input.dojoComboBox', value);
          await wb.page.waitFor('div[resultname="' + value + '"]');
          await wb.page.click('div[resultname="' + value + '"]');
        },
        text: async function(selector, value) {
          wb.log.debug('inside text: ' + selector + ' | ' + value);
          await wb.page.evaluate((selector) => {
            document.querySelector(selector).value = '';
          }, selector);
          await wb.page.type(selector, value);
        },
        button: async function(selector) {
          wb.log.debug('inside button: ' + selector);
          await wb.page.click(selector);
        },
        checkbox: async function(selector, value) {
          wb.log.debug('inside checkbox: ' + selector + ' | ' + value);
          await wb.page.evaluate((selector, value) => {
            let el = document.querySelector(selector);
            let event = new Event('click', {bubbles: true});
            el.checked = value;
            el.dispatchEvent(event);
          }, selector, value);
        },
        select: async function(selector, value) {
          wb.log.debug('inside select: ' + selector + ' | ' + value);
          await wb.page.evaluate((selector, value) => {
            let el = document.querySelector(selector);
            el.value = value;
            let event = new Event('change', {bubbles: true});
            event.simulated = true;
            el.dispatchEvent(event);
          }, selector, value);
        },
        checkbox_group: async function(selector, value) {
          wb.log.debug('inside checkbox_group: ' + selector + ' | ' + value);

          if(Array.isArray(value)) {
            for(let i = 0; i <= value.length; i++) {
              await wb.page.click(selector + '[value="' + value[i] + '"]');
            }
          } else {
            await wb.page.click(selector + '[value="' + value + '"]');
          }
        },
        radio_buttons: async function(selector, value) {
          wb.log.debug('inside radio_buttons: ' + selector + ' | ' + value);

          await wb.page.evaluate((selector, value) => {
            let el = document.querySelector(selector);
            let event = new Event('click', {bubbles: true});
            el.checked = value;
            el.dispatchEvent(event);
          }, selector, value);
        },
        textarea: async function(selector, value) {
          wb.log.debug('inside textarea: ' + selector + ' | ' + value);
          await wb.page.evaluate((selector) => {
            document.querySelector(selector).value = '';
          }, selector);
          await wb.page.type(selector, value);
        },
      };

      // Check for required arguements have been passed.
      if(!args.has('type')) {
        throw new Error({type: 'Input missing `type`',
                         args: args, obj: input});
      } else {
        if(this.actions.has(args.type)) {
          wb.log.debug('Recognized input type: ' + args.type);
          this.action = this.actions[args.type];
          this.check = this.checks[args.type];
        } else {
          throw new Error({type: 'Unrecognized input type: ' + args.type,
                           args: args, obj: obj});
        }
      }

      input.execute = async function(value = null, times = 3, interval = 250) {
        return await async.retry({times: times, interval: interval},
          async function(callback) {
            try {
              wb.log.debug('Attempting data entry...');
              await wb.page.waitFor(input.selector);
              wb.log.debug('Selector found');
              await input.action(input.selector, value);
              wb.log.debug(await input.check(input.selector, value));
              let check = await input.check(input.selector, value);
              if(!check) {
                throw new Error('data entry failed');
              } else {
                return 'data entry suceeded';
              }
            } catch(err) {
              throw err;
            }
          },
          function(err, result) {
            if(err !== null) {
              wb.log.error('Data input failed');
              input.emit('failed', err);
            } else {
              wb.log.debug('Data input success');
              input.emit('complete');
            }
        });
      };

      EventEmitter.call(input);
    } catch(err) {
      throw err;
//      input.emit('error', err)
    }
  }
  util.inherits(Input, EventEmitter);

  return Input;
};
