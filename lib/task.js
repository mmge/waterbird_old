const EventEmitter = require('events').EventEmitter;
const async = require('async');
const util = require('util');
const fs = require('fs');
const yaml = require('js-yaml');


module.exports = function(wb) {
  const Page = require('./page.js')(wb);

  /**
   * Task - Task Object Constructor
   * @constructor
   * @param  {type} args arguments used to construct the task object
   * @yields {object} an object that represents a waterbird task
   */
  function Task(args) {
    try {
      wb.log.verbose('New Task Initialized', args);

      this.on('error', function(error) {
        wb.log.error('Task Error:', error);
      });

      let task = this;
      if(args.has('name')) {
        let file_path = './tasks/' + args.name + '.yaml';
        if(!fs.existsSync(file_path)) {
          throw new Error('task does not exist: ' + args.name);
        } else {
          if(args.has('data')) {
            task.data = args.data;
          } else {
            throw new Error('task missing `data` arg');
          }
          task.config = yaml.safeLoad(fs.readFileSync(file_path));
          task.config.url = task.config.url || '';
          task.config.name = task.config.name || args.name;
          if(!task.config.has('pages')) {
            wb.log.debug('Single page task identified');
            task.config.pages = [JSON.parse(JSON.stringify(task.config))];
            delete task.config.pages[0].url;
          }
          task.queue = async.queue(function(config, callback) {
            let page = new Page({config: config, data: task.data});
            page.on('complete', function() {
              setTimeout(callback, 100);
            });
            page.execute();
          });
          task.queue.drain = async function() {
            wb.log.debug('DRAIN');
            if(task.config.has('wait')) {
              await wb.page.waitFor(task.config.wait);
            }
            task.emit('complete');
          };
          task.queue.error = function(err) {
            throw err;
          };
        }

        task.execute = async function() {
          wb.log.debug('Beginning task: ' + task.config.name);
          let complete = true;
          let missing = [];
          for(let i = 0; i < task.config.pages.length; i++) {
            let page = task.config.pages[i];
            for(let j = 0; j < page.steps.length; j++) {
              let step = page.steps[j];
              let required = step.has('required');
              if(required && !task.data.has(step.source)) {
                complete = false;
                missing.push(step.source, function(err) {
                  console.log('Page Complete Callback!!!!!!!!!');
                });
              }
            }
          }
          if(!complete) {
            wb.log.verbose('Task data incomplete: ' + task.config.name);
          } else {
            wb.log.verbose('Task data complete: ' + task.config.name);
            if(task.config.has('url')) {
              let url = wb.get('base_url') + task.config.url;
              wb.log.debug('Redirecting to: ' + url + ' - ' + task.config.name);
              await wb.page.goto(url);
            }
            task.queue.push(task.config.pages);
          }
        };
      } else {
        throw new Error('Task missing `name` arg');
      }
      if(args.has('data')) {
        task.data = args.data;
      } else {
        throw new Error('Task missing `data` arg');
      }

      EventEmitter.call(this);
    } catch(err) {
      throw err;
    }
  }
  util.inherits(Task, EventEmitter);

  return Task;
};
