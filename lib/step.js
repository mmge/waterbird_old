const EventEmitter = require('events').EventEmitter;
const util = require('util');

module.exports = function(wb) {
  const Input = require('./input.js')(wb);


  /**
   * Step - Step object Constructor
   * @constructor
   * @param  {object} args arguments used to construct the Step object
   * @yields {object} an object that represents a waterbird Steo
   */
  function Step(args) {
    try {
      wb.log.verbose('New Step Initialized', args);

      let step = this;

      if(args.has('config')) {
        step.config = args.config;
      } else {
        throw new Error('Step missing `config` arg', args);
      }
      if(args.has('data')) {
        step.data = args.data;
      } else {
        throw new Error('Step missing `data` arg', args);
      }

      step.execute = async function() {
        if(step.data.has(step.config.source) |
           step.config.has('value') |
           step.config.type === 'button') {
             wb.log.debug('Step Needed');

             let value = await step.config.value ||
                               step.data[step.config.source] ||
                               null;
             if(step.config.has('code')) {
               value = await step.config.code[value];
             }
             let input = new Input(step.config);
             input.on('complete', async function() {
               if(step.config.has('wait')) {
                 wb.log.debug('Waiting for: ' + step.config.wait);
                 await wb.page.waitFor(step.config.wait);
               }
               step.emit('complete');
               wb.emit('step_complete', step);
             });
             input.on('failed', function() {
               step.emit('failed');
             });
             return await input.execute(value);
           } else {
             wb.log.debug('Step skipped');
             step.emit('skipped');
             step.emit('complete');
             return {
               msg: 'input skipped',
               selector: selector,
               value: value,
             };
           }
      };

      EventEmitter.call(this);
    } catch(err) {
      throw err;
    }
  };
  util.inherits(Step, EventEmitter);

  return Step;
};
