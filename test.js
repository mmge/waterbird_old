"use strict";
const puppeteer = require("puppeteer");
const winston = require('winston');
// Helper for finding keys in an object
Object.prototype.has = function(key) {
  return Object.keys(this).indexOf(key) > -1;
};
let test = async function() {
  let wb = {};
  wb.log = winston.createLogger({
    level: "info",
    format: winston.format.json(),
    transports: [
      new winston.transports.File({filename: "./logs/error.log", level: "error"}),
      new winston.transports.File({filename: "./logs/combined.log"})
    ]
  })
  // When debug === true output all logging to the console
    wb.log.add(new winston.transports.Console({
      level: "debug",
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp: true,
      format: winston.format.simple()
    }));

    wb.browser = await puppeteer.launch({
      headless: false,
      devtools: true,
      ignoreHTTPSErrors: true
    });
    wb.page = await wb.browser.newPage();
    await wb.page.setViewport({
      width: 1920,
      height: 1080
    });
    wb.log.debug("testing");

  //  await wb.page.goto("https://www.bing.com");

  //  const Input = require('./lib/input.js')(wb);
  //  const Step = require('./lib/step.js')(wb);
    const Page = require('./lib/page.js')(wb);

    let p = {
      name: "Bing test",
      url: "https://www.bing.com",
      steps: [
        {
          type: 'text',
          id: 'sb_form_q',
          source: 'search'
        },
        {
          type: 'button',
          id: 'sb_form_go'
        }
      ]
    };
    let d = {search: 'google'}
    let page = new Page({config: p, data: d});
    await page.execute();


/*
    let s = new Step({config: {type: 'text', id: "sb_form_q", source: 'search'}, data: {search: 'google'}});
    s.on('complete', function() {
      let t = new Step({config: {type: 'button', id: 'sb_form_go'}, data: {}});
      t.on('complete', function() {
        console.log("It worked!");
      })
      t.execute();
    })
    await s.execute();
    let s = new Input({type: 'text', id: "sb_form_q"});
    await s.execute("google");
    let t = new Input({type: 'button', id: "sb_form_go"});
    await t.execute();
*/

}

test();
