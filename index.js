'use strict';
const Waterbird = require('./lib/waterbird.js');
const request = require('request');
const wait = require('event-stream').wait;

const api_url = 'https://apps.medgen.iupui.edu/rsc/test_data/';
const waterbird = new Waterbird();

request(api_url + 'subject')
  .pipe(wait(async function(err, data) {
    data = JSON.parse(data);
    await waterbird.initialize();
    await waterbird.login();
    await waterbird.addTask(data);
//    await waterbird.close(10000);
}));
